<!DOCTYPE html>
<html lang="en">
   <head>
   <link rel="apple-touch-icon" sizes="180x180" href="../assets/media/icon/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="../assets/media/icon/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="../assets/media/icon/favicon-16x16.png">
      <link rel="manifest" href="/site.webmanifest">
      <meta name="msapplication-TileColor" content="#da532c">
      <meta name="theme-color" content="#ffffff">
   <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="C# / MS-Access - Car records application project">
      <meta name="author" content="John Crawley">
      <title>Car Records Project</title>
      <!--CSS-->
      <!--BOOTSTRAP'S CSS-->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <!-- GOOGLE STYLESHEET -->
      <link href="https://fonts.googleapis.com/css?family=Playfair&#43;Display:700,900&amp;display=swap" rel="stylesheet">
      <!--CUSTOM CSS-->
      <link href="../assets/css/main.css" type="text/css" rel="stylesheet">
      <!--BOOTSTRAP JS-->
      <!-- Latest compiled and minified JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
   </head>
   <!--BODY-->
   <body>
   <!--WEBSITE CONTAINER-->
      <div class="container">
         <!--HEADER/NAVIGATION-->
         <header class="blog-header py-3">
            <nav class="navbar navbar-expand-lg navbar-light">
               <a class="navbar-brand" href="../index.php">JOHN CRAWLEY</a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
                     <li class="nav-item">
                        <a class="nav-link" href="../index.php">Home</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="../about.php">About</a>
                     </li>
                     <li class="nav-item active">
                        <a class="nav-link" href="../projects.php">Projects</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="../contact.php">Contact</a>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
         <!--CONTENT BODY-->
         <main class="container">




         <!-- PROJECT LIST USING CARDS -->
         <div class="card">
            <div class="card-header">
               <p style="float: left;">C# Project</p>
               <p style="float: right;">2019</p>
            </div>
            <div class="card-body">
               <div class="row">
               <!--GIF-->
                  <div class="col-lg-12">
                  <img src="../assets/media/gifs/card-record-create-update.gif" class="img-fluid" alt="GIF car records create and update">
                  </div>
               </div>
               <div class="row">
                  <!--body/info/button-->
                  <div class="col-lg-12">
                     <h5 class="card-title">Car Records</h5>
                     <p class="card-text">
                     During my the time I studied at <a href="https://www.dfei.ie/">DFEi</a>, 
                     I was tasked with creating a CRUD (Create, Read, Update, Delete) application using C# 
                     as the language of choice, the application uses CRUD operations with Microsoft Access as the database of choice to create a car dealership application. 
                     Where a dealership can add, edit, update or delete a record in the application.
                     </p>
                     <br><br>
                     I also created an option in the help tab in the application to give instructions on how to use the application. 
                     
                     <img src="../assets/media/images/projects/car-record/help-popup.jpg" class=" img-fluid" alt="GIF car records create and update">

                     <br><br>
                     <a href="https://github.com/JohnMichaelFarrell/CarRecordApplication" class="btn btn-light">Visit GitHub</a>
                  </div>
               </div>     
            </div>
         </div>





















         </main>
         <!-- FOOTER -->
         <footer class="footer">
         <div class="footer-content">
                  <!-- 
                  NOTE:
                  Change 2021 to below
                  <php echo date("Y"); ?>
                  -->
                  <p class="footer-text">John Crawley &copy; 2022</p>
                  <!-- LINKEDIN LINK-->
                  <a href="https://www.linkedin.com/in/johnmichaelcrawley/">
                     <img src="../assets/media/images/icons/linkedin.svg" alt="LinkedIn icon">
                  </a>
                  <!-- INSTAGRAM LINK-->
                  <a href="https://www.instagram.com/accounts/login/?next=/johnmichaelcrawley/">
                     <img src="../assets/media/images/icons/instagram.svg" alt="Instagram icon">
                  </a>
                  <!-- TWITTER -->
                  <a href="http://twitter.com/johnmcrawley">
                     <img src="../assets/media/images/icons/twitter.svg" alt="Twitter icon">
                  </a>
                  <!-- YOUTUBE LINK -->
                  <a href="https://www.youtube.com/channel/UCzezRDp2uX3SWMztsjkFrTA">
                     <img src="../assets/media/images/icons/youtube.svg" alt="YouTube icon">
                  </a>
                  <!-- GITHUB LINK-->
                  <a href="http://github.com/johnmichaelcrawley/">
                     <img src="../assets/media/images/icons/github.svg" alt="GitHub icon">
                  </a>
                  <!-- MAIL LINK -->
                  <a href="mailto:contact@johncrawley.ie">
                  <img src="../assets/media/images/icons/mail.svg" alt="Email icon">
                  </a>
                  <!-- RETURN TO TOP OF PAGE -->
                  <a class="back-to-top" href="#">
                     <p class="footer-text">Back to top</p>
                  </a>
         </div>     
         </footer>
         <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
         <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
         <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      </div>
   </body>
</html>