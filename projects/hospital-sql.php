<!DOCTYPE html>
<html lang="en">
   <head>
   <link rel="apple-touch-icon" sizes="180x180" href="../assets/media/icon/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="../assets/media/icon/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="../assets/media/icon/favicon-16x16.png">
      <link rel="manifest" href="/site.webmanifest">
      <meta name="msapplication-TileColor" content="#da532c">
      <meta name="theme-color" content="#ffffff">
   <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="MySQL - Hospital Database project">
      <meta name="author" content="John Crawley">
      <title>Hospital Database Project</title>
      <!--CSS-->
      <!--BOOTSTRAP'S CSS-->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <!-- GOOGLE STYLESHEET -->
      <link href="https://fonts.googleapis.com/css?family=Playfair&#43;Display:700,900&amp;display=swap" rel="stylesheet">
      <!--CUSTOM CSS-->
      <link href="../assets/css/main.css" type="text/css" rel="stylesheet">
      <!--BOOTSTRAP JS-->
      <!-- Latest compiled and minified JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
   </head>
   <!--BODY-->
   <body>
   <!--WEBSITE CONTAINER-->
      <div class="container">
         <!--HEADER/NAVIGATION-->
         <header class="blog-header py-3">
            <nav class="navbar navbar-expand-lg navbar-light">
               <a class="navbar-brand" href="../index.php">JOHN CRAWLEY</a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
                     <li class="nav-item">
                        <a class="nav-link" href="../index.php">Home</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="../about.php">About</a>
                     </li>
                     <li class="nav-item active">
                        <a class="nav-link" href="../projects.php">Projects</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="../contact.php">Contact</a>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
         <!--CONTENT BODY-->
         <main class="container">
         <!-- PROJECT LIST USING CARDS -->
         <div class="card">
            <div class="card-header">
               <p style="float: left;">MySQL Project</p>
               <p style="float: right;">2020</p>
            </div>
            <div class="card-body">
               <div class="row">
               <!--image-->
               <div class="col-lg-12">
                  <img src="../assets/media/images/projects/hospital-ERD-diagram.jpg" class="img-thumbnail img-fluid" alt="ERD diagram of the database">
                  </div>
               </div>
               <div class="row">
                  <!--body/info/button-->
                  <div class="col-lg-12">
                     <h5 class="card-title">Hospital Database</h5>
                     <p class="card-text">
                     During my Introduction to Databases at National College of Ireland, 
                     during a group project, we created a snowflake schema involving hospital data and information consisting of
                     <ul>
                        <li>MySQL - Used for the database</li>
                        <li>SQL Workbench - Used to write MySQL scripts and queries</li>
                        <li>Draw.IO - To draw up diagram such as an E.R.D</li>
                     </ul>
                     We created multiple tables and records for the hospital database, the tables we created were:
                     <ul>
                        <li>Employees</li>
                        <li>Doctors</li>
                        <li>Nurses</li>
                        <li>Secretaries</li>
                        <li>Medical Record</li>
                        <li>Appointments</li>
                        <li>Department</li>
                        <li>Prescriptions</li>
                        <li>Patients</li>
                        <li>Payments</li>
                     </ul>
                     Once the database has been created and completed with the tables and records, we then created four queries
     
                 
                     </p>
                     <a href="https://github.com/JohnMichaelFarrell/HospitalDatabase" class="btn btn-light">Visit GitHub</a>
                  </div>
               </div>     
            </div>
         </div>





















         </main>
         <!-- FOOTER -->
         <footer class="footer">
         <div class="footer-content">
                  <!-- 
                  NOTE:
                  Change 2021 to below
                  <php echo date("Y"); ?>
                  -->
                  <p class="footer-text">John Crawley &copy; 2022</p>
                  <!-- LINKEDIN LINK-->
                  <a href="https://www.linkedin.com/in/johnmichaelcrawley/">
                     <img src="../assets/media/images/icons/linkedin.svg" alt="LinkedIn icon">
                  </a>
                  <!-- INSTAGRAM LINK-->
                  <a href="https://www.instagram.com/accounts/login/?next=/johnmichaelcrawley/">
                     <img src="../assets/media/images/icons/instagram.svg" alt="Instagram icon">
                  </a>
                  <!-- TWITTER -->
                  <a href="http://twitter.com/johnmcrawley">
                     <img src="../assets/media/images/icons/twitter.svg" alt="Twitter icon">
                  </a>
                  <!-- YOUTUBE LINK -->
                  <a href="https://www.youtube.com/channel/UCzezRDp2uX3SWMztsjkFrTA">
                     <img src="../assets/media/images/icons/youtube.svg" alt="YouTube icon">
                  </a>
                  <!-- GITHUB LINK-->
                  <a href="http://github.com/johnmichaelcrawley/">
                     <img src="../assets/media/images/icons/github.svg" alt="GitHub icon">
                  </a>
                  <!-- MAIL LINK -->
                  <a href="mailto:contact@johncrawley.ie">
                  <img src="../assets/media/images/icons/mail.svg" alt="Email icon">
                  </a>
                  <!-- RETURN TO TOP OF PAGE -->
                  <a class="back-to-top" href="#">
                     <p class="footer-text">Back to top</p>
                  </a>
         </div>     
         </footer>
         <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
         <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
         <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      </div>
   </body>
</html>