<!DOCTYPE html>
<html lang="en">
   <head>
   <link rel="apple-touch-icon" sizes="180x180" href="assets/media/icon/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="assets/media/icon/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="assets/media/icon/favicon-16x16.png">
      <link rel="manifest" href="/site.webmanifest">
      <meta name="msapplication-TileColor" content="#da532c">
      <meta name="theme-color" content="#ffffff">
   <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="A personal website by John Crawley to share projects, career experience all in one place">
      <meta name="author" content="John Crawley">
      <title>Contact</title>
      <!--CSS-->
      <!--BOOTSTRAP'S CSS-->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <!-- GOOGLE STYLESHEET -->
      <link href="https://fonts.googleapis.com/css?family=Playfair&#43;Display:700,900&amp;display=swap" rel="stylesheet">
      <!--CUSTOM CSS-->
      <link href="assets/css/main.css" type="text/css" rel="stylesheet">
   </head>
   <!--BODY-->
   <body>
   <!--WEBSITE CONTAINER-->
      <div class="container">
         <!--HEADER/NAVIGATION-->
         <header class="blog-header py-3">
            <nav class="navbar navbar-expand-lg navbar-light ">
               <a class="navbar-brand" href="index.php">JOHN CRAWLEY</a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
                     <li class="nav-item">
                        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="about.php">About</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="projects.php">Projects</a>
                     </li>
                     <li class="nav-item active">
                        <a class="nav-link" href="contact.php">Contact</a>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
         <!--CONTENT BODY-->
         <main class="container">
            <!--CONTACT FORM-->
            <div class="contact">
            <h4 class="text-center">Contact Form</h4>
         <!--CONTACT FORM-->
         <form 
         id="contact-form"
         method="POST" 
         action="assets/processing/mail_form.php" 
         class="row g-3" 
         >
            <!--FIRST AND LAST NAME-->
            <!--FIRST NAME-->
            <div class="col-md-6">
               <label for="validationDefault01" class="form-label">First name</label>
               <input name="first_name" type="text" class="form-control" id="validationDefault01" placeholder="John" required>
            </div>
            <!--LAST NAME-->
            <div class="col-md-6">
               <label for="validationDefault02" class="form-label">Last name</label>
               <input name="last_name" type="text" class="form-control" id="validationDefault02" placeholder="Smith" required>
            </div>
            <!--EMAIL-->
            <div class="col-lg-12">
               <label for="exampleInputEmail1" class="form-label">Email address</label>
               <input name="email" type="email" class="form-control" id="exampleInputEmail1" placeholder="JohnSmith@Gmail.com" required>
            </div>            
            <!--REASON FOR CONTACT -->
            <div class="col-lg-12">
               <label for="contact-reason" class="form-label">Reason for contact</label>
               <select  id="contact-reason" name="contact-reason" class="form-select form-select-lg" onchange="showBugSelection(this)">
                  <option>General / Question</option>
                  <option>Hiring / Business Enquiry</option>
                  <option>Project Question</option>
                  <option>Bug / Issue Found</option>
               </select>
            </div>
            <!--BUG IN SOFTWARE OPTION (DOESN'T DISPLAY UNTIL SELECTED FOR REAOSN FOR CONTACT) -->
            <div id="bug-selection"  style="display:none;" class="col-lg-12">
               <label for="bug-selection" class="form-label">Reason for contact</label>
               <select id="bug-select" name="bug-selection" class="form-select form-select-lg">
                  <option value="" selected disabled>-- Select an option --</option>
                  <option value="Website">Website</option>
                  <option value="GitHub Project">GitHub Project</option>
               </select>
               <p>Please be as detailed as you can while explaining below.</p>
            </div>
      <!--
         Script to display bug selection if 
         selected in the reason for contact
      -->
      <script type="text/javascript">
      function showBugSelection(select)
      {
         // Store the selected value in a variable
         const reasonValue = document.getElementById('contact-reason');
         const bugValue = document.getElementById('bug-selection');

         // Check if the value is bug in software
         if (reasonValue.value == "Bug / Issue Found" )
         {
            //If TRUE then show the bug dropdown 
            //console.log('✅ element is a select dropdown'); //test
            document.getElementById('bug-selection').style.display = "block";
            //Make the bug select dropdown required
            $("#bug-select").attr('required', 'required');
            document.getElementById("bug-select").required = true;
         }
         else
         {
            //Remove bug dropdown and remove reqired
            document.getElementById('bug-selection').style.display = "none";
            $("#bug-select").removeAttr('required');
         }
      }      
      </script>
            <!--EMAIL BODY MESSAGE-->  
            <div class="col-lg-12">
               <label for="exampleFormControlTextarea1" class="form-label">Message</label>
               <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="10" placeholder="Your message..." required></textarea>
            </div>   
         <!--SUBMIT-->
         <div class="col-lg-12">
            <button id="btn-submit" type="submit" class="btn btn-primary submit-form">Submit</button>
         </div>
         </form>
         </div>
         </main>
         <!-- FOOTER -->
         <footer class="footer">
         <div class="footer-content">
                  <!-- 
                  NOTE:
                  Change 2021 to below
                  <php echo date("Y"); ?>
                  -->
                  <p class="footer-text">John Crawley &copy; 2022</p>
                  <!-- LINKEDIN LINK-->
                  <a href="https://www.linkedin.com/in/johnmichaelcrawley/">
                     <img src="assets/media/images/icons/linkedin.svg" alt="LinkedIn icon">
                  </a>
                  <!-- INSTAGRAM LINK-->
                  <a href="https://www.instagram.com/accounts/login/?next=/johnmichaelcrawley/">
                     <img src="assets/media/images/icons/instagram.svg" alt="Instagram icon">
                  </a>
                  <!-- TWITTER -->
                  <a href="http://twitter.com/johnmcrawley">
                     <img src="assets/media/images/icons/twitter.svg" alt="Twitter icon">
                  </a>
                  <!-- YOUTUBE LINK -->
                  <a href="https://www.youtube.com/channel/UCzezRDp2uX3SWMztsjkFrTA">
                     <img src="assets/media/images/icons/youtube.svg" alt="YouTube icon">
                  </a>
                  <!-- GITHUB LINK-->
                  <a href="http://github.com/johnmichaelcrawley/">
                     <img src="assets/media/images/icons/github.svg" alt="GitHub icon">
                  </a>
                  <!-- MAIL LINK -->
                  <a href="mailto:contact@johncrawley.ie">
                  <img src="assets/media/images/icons/mail.svg" alt="Email icon">
                  </a>
                  <!-- RETURN TO TOP OF PAGE -->
                  <a class="back-to-top" href="#">
                     <p class="footer-text">Back to top</p>
                  </a>
         </div>     
         </footer>
         <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
         <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
         <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
     
      
      
      </div>
   </body>
</html>