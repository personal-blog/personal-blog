<!DOCTYPE html>
<html lang="en">
   <head>
   <link rel="apple-touch-icon" sizes="180x180" href="assets/media/icon/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="assets/media/icon/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="assets/media/icon/favicon-16x16.png">
      <link rel="manifest" href="/site.webmanifest">
      <meta name="msapplication-TileColor" content="#da532c">
      <meta name="theme-color" content="#ffffff">
   <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="A personal website by John Crawley to share projects, career experience all in one place">
      <meta name="author" content="John Crawley">
      <title>About</title>
      <!--CSS-->
      <!--BOOTSTRAP'S CSS-->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <!-- GOOGLE STYLESHEET -->
      <link href="https://fonts.googleapis.com/css?family=Playfair&#43;Display:700,900&amp;display=swap" rel="stylesheet">
      <!--CUSTOM CSS-->
      <link href="assets/css/main.css" type="text/css" rel="stylesheet">
      <!--BOOTSTRAP JS-->
      <!-- Latest compiled and minified JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
   </head>
   <!--BODY-->
   <body>
   <!--WEBSITE CONTAINER-->
      <div class="container">
         <!--HEADER/NAVIGATION-->
         <header class="blog-header py-3">
            <nav class="navbar navbar-expand-lg navbar-light ">
               <a class="navbar-brand" href="index.php">JOHN CRAWLEY</a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
                     <li class="nav-item">
                        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                     </li>
                     <li class="nav-item active">
                        <a class="nav-link" href="about.php">About</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="projects.php">Projects</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="contact.php">Contact</a>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
         <!--CONTENT BODY-->
         <main class="container">
         <h4 class="text-center">About</h4>
         <!-- SECTIONS ABOUT ME -->
        <!--WHO AM I? - SECTION -->
        <section>
            <div class="about-section container p-3 mb-2 bg-light text-dark">
                <h1 class="fw-bolder text-center">Who Am I?</h1> 
                <!--BOOTSTRAP COLUMN-->
                <div class="row">
                    <!--Image--> 
                    <div class="col-sm-6">
                        <img src="assets/media/images/about/28-10-2020-16-38-13.jpeg" class="crop-img img-fluid" alt="Photo of me">
                    </div>
                    <!--About Me-->
                    <div class="col-sm-6">
                        <h3>I'm John Crawley</h3>
                        <p>I'm a 27 year old, fourth year Computer Science student at <a href="https://www.ncirl.ie">NCI</a>. 
                        My hobbies / passions include; software development, learning Japanese, video-editing, playing guitar, 
                        researching topics etc.</p>            
                    </div>
                </div>
            </div>
        </section>
        <!--MISSION - SECTION -->
        <section>
            <div class="about-section container p-3 mb-2 bg-light text-dark">
                <h1 class="fw-bolder text-center">My Mission</h1>
                <!--BOOTSTRAP COLUMN-->
                <div class="row">
                    <!--INFO-->
                    <div class="col-12">
                       <div class="about-text-box">
                        <p>
                           My current mission is to get to a proficient level of the Japanese language by taking
                           the <a href="https://www.jlpt.jp/e/">Japanese Language Proficiency Test</a> and reaching
                           N1 level. I also want to create and publish multiple software development projects using 
                           various languages 
                           and deploying them to different operating systems such as Windows, MacOS, iPhone, Android etc, 
                           improving myself in various ways such as my career and health, 
                           improve personal, interpersonal and technical skills.</p>
                       </div>
                    </div>
                </div>
            </div>
        </section>
        <!--ACOMPLISHMENTS - SECTION -->
        <section>
            <div class="about-section container p-3 mb-2 bg-light text-dark">
                <h1 class="fw-bolder text-center">My acomplishments</h1>
                <!--BOOTSTRAP COLUMN-->
                <div class="row">
                    <!--INFO--> 
                    <div class="col-12">
                    <div class="about-text-box">
                        <ul>
                           <li>High results on projects related to developing applications in my degree</li>
                           <li>Lost over 8.9 stone [124.6lbs | 56.5176kg] </li>
                           <li> Successfully helped volunteer at <a href="https://experiencejapan.ie">Experience Japan</a> in April 2022</li>
                           <li>I organize <a href="https://yotsuba.ie/en">Yotsuba</a>, a Japanese/English language exchange meet up group</li>
                           <!--   <li>Reaching N4 level proficiency in <a href="https://www.jlpt.jp/e/index.html">JLPT</a> </li> -->
                        </ul>
                     </div>
                    </div>
                </div>
            </div>
        </section>
        <!--SOFTWARE DEVELOPMENT -->
         <!--ACOMPLISHMENTS - SECTION -->
         <section>
            <div class="about-section container p-3 mb-2 bg-light text-dark">
                <h1 class="fw-bolder text-center">Software Development</h1>
                <!--BOOTSTRAP COLUMN-->
                <div class="row">
                    <!--INFO--> 
                    <div class="col-12">
                    <div class="about-text-box">
                       <p>
                        I'm a highly passionate and motivated software developer who has experience working in Java, Python, 
                        C# and Swift 5+ through various courses and projects.
                        I also have real world experience from my internship at <a href="https://cylynt.com">Cylynt</a> taking part
                        in a DevOps team using such tecnhologies like Python, AWS, Oracle Apex etc. 
                        along with gaining excellent teamwork skills. 
                        Through my courses and undertaking my undergrad degree doing Computer Science, I have gained valuable 
                        knowledge of algorithms and data structures, along with working in Agile development / S.D.L.C.                           
                        </p>
                     </div>
                     </div>
                </div>
            </div>
        </section>
        <!--SOFTWARE DEVELOPMENT -->
         <!--CAREER - SECTION -->
         <section>
            <div class="about-section container p-3 mb-2 bg-light text-dark">
                <h1 class="fw-bolder text-center">Career Experience</h1>
                <!--BOOTSTRAP COLUMN-->

                <!--ROW 1-->
                <div class="row">
                   <!--LOGO-->
                  <div class="col-md-6">
                     <img src="assets/media/images/about/logo/Cylynt-Logo.png" class="logo img-fluid img-thumbnail" alt="Cylynt Logo">
                  </div>
                  <!--COPMANY-->
                  <div class="col-md-6">
                     <p>
                        Position: DevOps Intern - 
                        <br>
                        For my six month internship at Cylynt, 
                        I was tasked with creating Python scripts such as an automation script for converting data, testing scripts along with working with Amazon Web Services (AWS) with Python with S3 Bucket. 
                        I was tasked with researching into areas of AWS Athena, Docker etc. I also worked in the frontend of the application and got experience working with 
                        Oracle Apex. 
                     </p>
                  </div>
                </div>
               <hr> <!-- HR LINE -->
                 <!--ROW 2-->
                 <div class="row">
                   <!--LOGO-->
                  <div class="col-md-6">
                  <img src="assets/media/images/about/logo/Netflix-Logo.png" class="logo img-fluid img-thumbnail" alt="Cylynt Logo">
                  </div>
                  <!--COPMANY-->
                  <div class="col-md-6">
                     <p>
                        Position: Road Crew - 
                        <br> 
                        For my time on road crew, I have coordinated multiple traffic containing actors, 
                        props etc down either a one way road or a t-junction and succesfully executing my job 
                        by using good and clear communication skills along with working under pressure during
                        high peak times of vehicles going up or down the roads. 
                     </p>
                  </div>
                </div>
            </div>
        </section>
         </main>
         <!-- FOOTER -->
         <footer class="footer">
         <div class="footer-content">
                  <p class="footer-text">John Crawley &copy; 2022</p>
                  <!-- LINKEDIN LINK-->
                  <a href="https://www.linkedin.com/in/johnmichaelcrawley/">
                     <img src="assets/media/images/icons/linkedin.svg" alt="LinkedIn icon">
                  </a>
                  <!-- INSTAGRAM LINK-->
                  <a href="https://www.instagram.com/accounts/login/?next=/johnmichaelcrawley/">
                     <img src="assets/media/images/icons/instagram.svg" alt="Instagram icon">
                  </a>
                  <!-- TWITTER -->
                  <a href="http://twitter.com/johnmcrawley">
                     <img src="assets/media/images/icons/twitter.svg" alt="Twitter icon">
                  </a>
                  <!-- YOUTUBE LINK -->
                  <a href="https://www.youtube.com/channel/UCzezRDp2uX3SWMztsjkFrTA">
                     <img src="assets/media/images/icons/youtube.svg" alt="YouTube icon">
                  </a>
                  <!-- GITHUB LINK-->
                  <a href="http://github.com/johnmichaelcrawley/">
                     <img src="assets/media/images/icons/github.svg" alt="GitHub icon">
                  </a>
                  <!-- MAIL LINK -->
                  <a href="mailto:contact@johncrawley.ie">
                  <img src="assets/media/images/icons/mail.svg" alt="Email icon">
                  </a>
                  <!-- RETURN TO TOP OF PAGE -->
                  <a class="back-to-top" href="#">
                     <p class="footer-text">Back to top</p>
                  </a>
         </div>     
         </footer>
         <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
         <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
         <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      </div>
   </body>
</html>