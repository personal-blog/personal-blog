<?php
// Variables // 
$errors = [];           // Store any errors in an array //
$errorMessage = '';     // Store an error message //
/*
 If the fields in the form aren't empty
 then we will try send them to the email address
 to be recieved.
*/
if (!empty($_POST)) 
{
    /*
    Store the data from the textfields 
    in a variable which will be used to be
    sent to an email address. 
    */
    $first_name = $_POST['first_name'];         // Store the first name of the writer
    $last_name = $_POST['last_name'];         // Store the first name of the writer
    $email = $_POST['email'];                 // Store the email address of the writer
    $message = $_POST['message'];             // Store the contents of the message
    /*
    Validate that the fields have data
    */

    /*
    if (empty($name)) {
        $errors[] = 'Name is empty';
        echo $errors;
    }

    if (empty($email)) {
        $errors[] = 'Email is empty';
        echo $errors;
    } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[] = 'Email is invalid';
        echo $errors;
    }

    if (empty($message)) {
        $errors[] = 'Message is empty';
        echo $errors;
    }*/



    /*
    IF no fields have errors 
    then we will process the data
    and send it to the email address
    */
    if (empty($errors)) 
    {
        /*
        Check what department ("contact reason") the user selected
        */
        switch ($_POST['contact-reason']) 
        {
            case 'General / Question':
                $toEmail = 'contact@johncrawley.ie';
                break;
            case 'Hiring / Business Enquiry':
                $toEmail = 'business@johncrawley.ie';
                break;
            case 'Project Question':
                $toEmail = 'project@johncrawley.ie';
                break;
            case 'Bug / Issue Found':
                $toEmail = 'bug-report@johncrawley.ie';
                break;
            default:
               $toEmail = 'contact@johncrawley.ie';
                break;
        }
        /*
        Found where the bug / issue is         
        */
        switch ($_POST['bug-selection']) 
        {
            case 'Website':
                $bug_issue = 'Website';
                break;
            case 'GitHub Project':
                $bug_issue = 'GitHub Project';
                break;
            default:
               $bug_issue = '';
                break;
        }






        /* 
        Store the name of the writer of the contact 
        to a session and send it to the thank you page
        Works for Safari but not Firefox.
        */
        session_start();
        $_SESSION['first_name'] = $_POST['first_name'];
        // Store the reason for contact as the subject//


        $emailSubject =  $_POST['contact-reason'];

        //Concat the bug / issue reason to the subject if it's not empty
        if ($bug_issue != '')
        {
            $emailSubject .= ': '.$bug_issue;
        }
      //  $bug_issue



        // Store the header info'//
        $headers = ['From' => $email, 'Reply-To' => $email, 'Content-type' => 'text/html; charset=iso-8859-1'];
        //Set the body of the email
        $bodyParagraphs = [$message];
        $body = join(PHP_EOL, $bodyParagraphs);
        //Send email then direct to a thank you page
        if (mail($toEmail, $emailSubject, $body, $headers)) 
        {
            // Print to the page it's redirecting
            echo "Redirecting...";
            /*
            Redirect page to thank-you.php.
            If the header doesn't work then 
            use javascript to force a redirect
            */
            header("Location:thank-you.php");
            echo "<script type='text/javascript'> document.location = 'thank-you.php'; </script>";
            die("Redirecting...");
        } 
        else
        {
            // Display any errors on the page//
            $errorMessage = 'Oops, something went wrong. Please try again later';
            echo $errorMessage;
        }
    } 
    else 
    {
        $allErrors = join('<br/>', $errors);
        $errorMessage = "<p style='color: red;'>{$allErrors}</p>";
        echo $errorMessage;
    }
}







?>