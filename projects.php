<!DOCTYPE html>
<html lang="en">
   <head>
   <link rel="apple-touch-icon" sizes="180x180" href="assets/media/icon/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="assets/media/icon/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="assets/media/icon/favicon-16x16.png">
      <link rel="manifest" href="/site.webmanifest">
      <meta name="msapplication-TileColor" content="#da532c">
      <meta name="theme-color" content="#ffffff">
   <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="A personal website by John Crawley to share projects, career experience all in one place">
      <meta name="author" content="John Crawley">
      <title>Projects</title>
      <!--CSS-->
      <!--BOOTSTRAP'S CSS-->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <!-- GOOGLE STYLESHEET -->
      <link href="https://fonts.googleapis.com/css?family=Playfair&#43;Display:700,900&amp;display=swap" rel="stylesheet">
      <!--CUSTOM CSS-->
      <link href="assets/css/main.css" type="text/css" rel="stylesheet">
      <!--BOOTSTRAP JS-->
      <!-- Latest compiled and minified JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
   </head>
   <!--BODY-->
   <body>
   <!--WEBSITE CONTAINER-->
      <div class="container">
         <!--HEADER/NAVIGATION-->
         <header class="blog-header py-3">
            <nav class="navbar navbar-expand-lg navbar-light ">
               <a class="navbar-brand" href="index.php">JOHN CRAWLEY</a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
                     <li class="nav-item">
                        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="about.php">About</a>
                     </li>
                     <li class="nav-item active">
                        <a class="nav-link" href="projects.php">Projects</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="contact.php">Contact</a>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
         <!--CONTENT BODY-->
         <main class="container">
         <!--Page header-->
         <h4 class="text-center">Featured Projects</h4>
         <!-- PROJECT: INDECISIVE CARD -->
         <div class="card">
            <div class="card-header">
               <p style="float: left;">iOS Project</p>
               <p style="float: right;">2022 - 2023</p>
            </div>
            <div class="card-body">
               <div class="row">
               <!--image-->
               <div class="col-md-6">
                  <img src="assets/media/images/home/spon-poster.jpg" class="img-thumbnail img-fluid" alt="ML/AI project example data">
               </div>
                  <!--body/info/button-->
                  <div class="col-md-6">
                     <h5 class="card-title">Spontaneous</h5>
                     <p class="card-text">
                     Spontaneous (Codename: PROJECT: INDECISIVE) is a six month project for my final year at <a href="https://ncirl.ie">NCI</a>.  
                     The project is an iOS project with MVVM architecture, the application allows you to find something random to do in your location.
                     The application will have two betas, a private beta for a closed group and a public beta that anyone can join in and this application will be released on the App Store in 2023.
                     </p>
                     <a href="projects/spontaneous.php" class="btn  btn-light">More Details</a>
                     <a href="http://spontaneous.johncrawley.ie" class="btn btn-light">Visit Website</a>
                     <a href="https://github.com/JohnMichaelCrawley/Spontaneous" class="btn btn-light">Visit GitHub</a>
                  </div>
               </div>     
            </div>
         </div>
         <!-- ML / AI PROJECT CARD -->
         <div class="card">
            <div class="card-header">
               <p style="float: left;">Python Project</p>
               <p style="float: right;">2022</p>
            </div>
            <div class="card-body">
               <div class="row">
                  <!--image-->
                  <div class="col-md-6">
                     <img src="assets/media/images/projects/ML-AI.jpg" class="img-thumbnail img-fluid" alt="ML/AI project example data">
                  </div>
                  <!--body/info/button-->
                  <div class="col-md-6">
                     <h5 class="card-title">Artificial Intelligence & Machine Learning Project</h5>
                     <p class="card-text">
                     For my third year project for machine learning / artificial intelligence project I used Python, two datasets (One for COVID-19 and another dataset for heart diease) and Juypter to:
                     <ol type="i">
                        <li>Use Logistic Regression & K-Nearest Neighbour (K-NN) classification algorithms</li>
                        <li>Measure performance of Linear Regression</li>
                     </ol>
                     <a href="projects/ml-ai-project.php" class="btn btn-light">More Details</a>
                     <a href="https://github.com/JohnMichaelFarrell/COVID-19-Heart-Disease-ML-" class="btn btn-light">Visit GitHub</a>
                  </div>
               </div>     
            </div>
         </div>
            <!-- HOSPITAL MYSQL PROJECT CARD -->
            <div class="card">
            <div class="card-header">
               <p style="float: left;">MySQL Project</p>
               <p style="float: right;">2020</p>
            </div>
            <div class="card-body">
               <div class="row">
                  <!--image-->
                  <div class="col-md-6">
                     <img src="assets/media/images/projects/hospital-ERD-diagram.jpg" class="img-thumbnail img-fluid" alt="ERD diagram of the database">
                  </div>
                  <!--body/info/button-->
                  <div class="col-md-6">
                     <h5 class="card-title">Hospital Database</h5>
                     <p class="card-text">
                     During my Introduction to Databases at National College of Ireland, 
                     during a group project, we created a snowflake schema involving hospital data and information using SQL Workbench such 
                     as tables for doctors, nurses, patients, department etc along with creating four queries of the 
                     database. 
                  </p>
                     <a href="projects/hospital-sql.php" class="btn btn-light">More Details</a>
                     <a href="https://github.com/JohnMichaelFarrell/HospitalDatabase" class="btn btn-light">Visit GitHub</a>
                  </div>
               </div>     
            </div>
         </div>
           <!-- IOS BLACKJACK PROJECT CARD  -->
           <div class="card">
            <div class="card-header">
               <p style="float: left;">Swift Project</p>
               <p style="float: right;">2020</p>
            </div>
            <div class="card-body">
               <div class="row">
                  <!--image-->
                  <div class="col-md-6">
                     <img src="assets/media/images/projects/ios-blackjack.jpg" class="img-thumbnail img-fluid" alt="Blackjack game">
                  </div>
                  <!--body/info/button-->
                  <div class="col-md-6">
                     <h5 class="card-title">iOS Blackjack Game</h5>
                     <p class="card-text">
                     During my summer break from National College of Ireland in my second year, 
                     I given myself the task to create a basic iOS blackjack game, I developed
                     the application using MVC architecture and building it with Swift 5 using Swift's
                     UIKit. 
                  </p>
                     <a href="projects/ios-blackjack.php" class="btn btn-light">More Details</a>
                     <a href="https://github.com/JohnMichaelFarrell/BlackjackGame" class="btn btn-light">Visit GitHub</a>
                  </div>
               </div>     
            </div>
         </div>
            <!-- CAR RECORDS PROJECT CARD -->
            <div class="card">
            <div class="card-header">
               <p style="float: left;">C# Project</p>
               <p style="float: right;">2019</p>
            </div>
            <div class="card-body">
               <div class="row">
                  <!--image-->
                  <div class="col-md-6">
                     <img src="assets/media/images/projects/Car-records.jpg" class="img-thumbnail img-fluid" alt="Card Records">
                  </div>
                  <!--body/info/button-->
                  <div class="col-md-6">
                     <h5 class="card-title">Car Records Application</h5>
                     <p class="card-text">
                     During my the time I studied at <a href="https://www.dfei.ie/">DFEi</a>, 
                     I was tasked with creating a CRUD (Create, Read, Update, Delete) application using C# 
                     as the language of choice, the application uses CRUD operations with Microsoft Access as the database of choice to create a car dealership application. 
                     Where a dealership can add, edit, update or delete a record in the application.
                  </p>
                     <a href="projects/car-records.php" class="btn btn-light">More Details</a>
                     <a href="https://github.com/JohnMichaelFarrell/CarRecordApplication" class="btn btn-light">Visit GitHub</a>
                  </div>
               </div>     
            </div>
         </div>
         <!-- PROJECT LIST USING CARDS -->
         <div class="card">
            <div class="card-header">
               <p style="float: left;">Java Project</p>
               <p style="float: right;">2015</p>
            </div>
            <div class="card-body">
               <div class="row">
                  <!--image-->
                  <div class="col-md-6">
                     <img src="assets/media/images/projects/airplane-calc.jpg" class="img-thumbnail img-fluid" alt="Blackjack game">
                  </div>
                  <!--body/info/button-->
                  <div class="col-md-6">
                     <h5 class="card-title">Airplane Distance Calculator</h5>
                     <p class="card-text">
                     During my 2015 course at Roslyn Park College studying software development, 
                     I was tasked with creating an application that has a airplane object and a destination object in Java where the application
                     calculates if a plane can reach the distance of the destination. Then the application will output if it can or not. If it can, it will return 
                     a message stating it can and the time it would take using the airplane selected. 
                     </p>
                     <a href="projects/airplane-distance.php" class="btn btn-light">More Details</a>
                     <a href="https://github.com/JohnMichaelFarrell/Airplane-Destination-Calculator" class="btn btn-light">Visit GitHub</a>
                  </div>
               </div>     
            </div>
         </div>
         </main>
         <!-- FOOTER -->
         <footer class="footer">
         <div class="footer-content">
                  <p class="footer-text">John Crawley &copy; 2022</p>
                  <!-- LINKEDIN LINK-->
                  <a href="https://www.linkedin.com/in/johnmichaelcrawley/">
                     <img src="assets/media/images/icons/linkedin.svg" alt="LinkedIn icon">
                  </a>
                  <!-- INSTAGRAM LINK-->
                  <a href="https://www.instagram.com/accounts/login/?next=/johnmichaelcrawley/">
                     <img src="assets/media/images/icons/instagram.svg" alt="Instagram icon">
                  </a>
                  <!-- TWITTER -->
                  <a href="http://twitter.com/johnmcrawley">
                     <img src="assets/media/images/icons/twitter.svg" alt="Twitter icon">
                  </a>
                  <!-- YOUTUBE LINK -->
                  <a href="https://www.youtube.com/channel/UCzezRDp2uX3SWMztsjkFrTA">
                     <img src="assets/media/images/icons/youtube.svg" alt="YouTube icon">
                  </a>
                  <!-- GITHUB LINK-->
                  <a href="http://github.com/johnmichaelcrawley/">
                     <img src="assets/media/images/icons/github.svg" alt="GitHub icon">
                  </a>
                  <!-- MAIL LINK -->
                  <a href="mailto:contact@johncrawley.ie">
                  <img src="assets/media/images/icons/mail.svg" alt="Email icon">
                  </a>
                  <!-- RETURN TO TOP OF PAGE -->
                  <a class="back-to-top" href="#">
                     <p class="footer-text">Back to top</p>
                  </a>
         </div>     
         </footer>
         <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
         <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
         <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      </div>
   </body>
</html>